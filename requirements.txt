appdirs==1.4.3
asn1crypto==0.24.0
astropy==2.0.11
astropy-healpix==0.4
astroquery==0.4.1.dev0
astroscrappy==1.0.8
atomicwrites==1.2.1
attrs==18.2.0
backports.functools-lru-cache==1.5
backports.shutil-get-terminal-size==1.0.0
beautifulsoup4==4.7.1
boto==2.49.0
ccdproc==1.3.0.post1
certifi==2018.11.29
cffi==1.11.5
chardet==3.0.4
cloudpickle==0.6.1
configparser==3.7.1
cryptography==2.5
cycler==0.10.0
dask==1.0.0
decorator==4.3.0
entrypoints==0.3
enum34==1.1.6
funcsigs==1.0.2
html5lib==1.0.1
idna==2.8
ipaddress==1.0.22
ipython==5.8.0
ipython-genutils==0.2.0
jdcal==1.4.1
keyring==17.1.1
kiwisolver==1.0.1
Mako==1.0.7
MarkupSafe==1.1.0
matplotlib==2.2.3
more-itertools==5.0.0
networkx==2.2
numpy==1.15.4
pandas==0.24.2
pathlib2==2.3.3
pexpect==4.6.0
photutils==0.4.1
pickleshare==0.7.5
Pillow==5.4.1
pkg-resources==0.0.0
pluggy==0.7.1
prompt-toolkit==1.0.15
ptyprocess==0.6.0
py==1.7.0
pycparser==2.19
pycuda==2017.1.1
pyds9==1.8.1
Pygments==2.3.1
pyparsing==2.3.1
pyraf==2.1.15
pytest==3.6.4
python-dateutil==2.7.5
pytools==2018.5.2
pytz==2018.9
PyWavelets==1.0.1
reproject==0.4
requests==2.21.0
scandir==1.9.0
scikit-image==0.14.1
scikit-learn==0.20.2
scipy==1.2.0
SecretStorage==2.3.1
simplegeneric==0.8.1
six==1.12.0
soupsieve==1.7.3
stsci.tools==3.4.13
subprocess32==3.5.3
toolz==0.9.0
traitlets==4.3.2
uncertainties==3.0.3
urllib3==1.24.1
wcwidth==0.1.7
webencodings==0.5.1
