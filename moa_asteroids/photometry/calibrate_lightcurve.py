
from moa_asteroids import constants
from moa_asteroids.util import get_subf_and_coordinates
from uncertainties import ufloat, umath

import numpy as np
import matplotlib.pyplot as plt
import pyds9
import os
import sys
import csv

# Constants for picking comparison stars
MIN_COMP_STARS = 18
MAX_COMP_STARS = 25
MAX_POS_MISMATCH = 0.5  # Pixels radius
MAX_MAG_ERROR = 0.1
MAX_MAG = 14  # Instrumental mags rather than actual magnitudes
MIN_COMP_STAR_PHOTOM_COUNT = 0  # Minimum photometry count for a reference star


comp_star_dict = {}


class ComparisonStar(object):
    def __init__(self, x, y, mag, flux=None):
        self.x = x
        self.y = y
        self.mag = mag
        self.flux = flux

    def __repr__(self):
        return "ComparisonStar({}, {}, {}, {})".format(
            self.x, self.y, self.mag, self.flux)


def calibrate_lightcurve(lightcurve, asteroid_id, magntiude_conversion=True,
                         graph=False):
    """ Takes a lightcurve in ADU values + frames and carefully turns
    these into differential magnitudes using the calculated reference
    magnitudes as well as the known magnitues from ians source

    Lightcurve: should be in the form
    [[date, frame, flux, fluxerr]]
    """

    for line in lightcurve:
        date, frame, flux, fluxerr = line
        run, field, filter, chip, subf = frame.split("-")

        if flux <= 1:
            print("Invalid flux value {}".format(flux))
            line.append(None)
            line.append(None)
            line.append(None)
            line.append(None)
            continue

        flux = ufloat(flux, fluxerr)
        comp_stars = get_comp_stars_flux(field, chip, subf, frame, asteroid_id)

        n = len(comp_stars)
        if n < MIN_COMP_STARS:
            print("Not enough comparison stars for frame {}".format(frame))
            line.append(None)
            line.append(None)
            line.append(None)
            line.append(None)
            continue

        comp_stars_flux = []
        comp_stars_mag = []

        for star in comp_stars:
            if star.flux:
                comp_stars_flux.append(star.flux)
                comp_stars_mag.append(star.mag)

        comp_stars_flux = np.array(comp_stars_flux)
        comp_stars_mag = np.array(comp_stars_mag)

        # See derivation somewhere
        # this is a standard calculation to give the differential
        # instrumental magnitude
        # honsetly I think this might be the least good part of all
        # of this
        # but I don't feel like I know enough to say either way
        # i think the best thing for this would be to do it and
        # then look at the results and then ask nick
        t = flux/comp_stars_flux
        t = np.array([umath.log10(x) for x in t])
        delta_m = -2.5 * t
        mag = comp_stars_mag + delta_m
        weights = np.array([1/x.s for x in mag])

        mag = weighted_average(mag, weights)

        line.append(mag.n)
        line.append(mag.s)

        if magntiude_conversion:
            a, b = get_transform_coefficents(field, chip)
            mag = a + b * mag
            line.append(mag.n)
            line.append(mag.s)

    if graph:
        lc = [x for x in lightcurve if x[-2] is not None]
        times = [x[0] for x in lc]
        mags = [x[-2] for x in lc]
        magerrs = [x[-1] for x in lc]
        plt.errorbar(times, mags, magerrs, fmt='o')
        plt.show()

    return lightcurve


def get_comp_stars_flux(field, chip, subf, frame, asteroid_id):
    """ Get a list of comparison stars with their flux and
    magnitudes for a given frame """
    comp_stars = get_comp_stars(field, chip, subf, asteroid_id)

    # Open flux file
    diff_flux = constants.DIFFERENCE_IMG_FLUX.format(
        asteroid_id=asteroid_id, frame=frame)
    fluxes = np.loadtxt(diff_flux)
    ref_flux = np.loadtxt(constants.DIFFERENCE_IMG_DIR.format(
        asteroid_id=asteroid_id) + "/ref.flux.calibrated{}_{}_{}".format(
            field, chip, subf))

    print("Initial stars: {}".format(len(comp_stars)))
    for star in comp_stars[:]:
        reference_flux, reference_flux_error = ref_flux[star.line]
        exposure_flux, exposure_flux_err = fluxes[star.line]

        if np.isnan(reference_flux_error) or np.isnan(exposure_flux_err):
            #  print("Bad Star: {}, expflux: {} refflux{}".format(
            #    star, exposure_flux, reference_flux))
            comp_stars.remove(star)
        elif reference_flux < MIN_COMP_STAR_PHOTOM_COUNT:
            # print("Bad Star: {}, expflux: {} refflux{}".format(
            #    star, exposure_flux, reference_flux))
            comp_stars.remove(star)
        elif abs(exposure_flux) > (abs(exposure_flux_err) +
                abs(reference_flux_error)):
            # NOTE(amelia): This means that either the difference imaging
            # was bad or that this is a variable star or other stuff was bad
            # print("Bad Star: {}, expflux: {} refflux{}".format(
            #    star, exposure_flux, reference_flux))
            comp_stars.remove(star)
        elif abs(reference_flux_error) > 1e5 or abs(exposure_flux_err) > 1e5:
            comp_stars.remove(star)
        else:
            star.flux = (ufloat(reference_flux, reference_flux_error) +
                         ufloat(0, exposure_flux_err))
    print("Final stars: {}".format(len(comp_stars)))
    return comp_stars


def weighted_average(input, weights):
    input = input * weights
    input = np.sum(input)
    return input/np.sum(weights)


def get_comp_stars(field, chip, subf, asteroid_id):
    """ Get the list of comparison stars for a given chip/field and asteroid

    Comparison stars will have instrumental magnitudes
    Returns a list of ComparisonStar objects
    """

    global comp_star_dict
    if "{}-{}-{}".format(field, chip, subf) in comp_star_dict:
        return comp_star_dict["{}-{}-{}".format(field, chip, subf)][:]
    filename = constants.COMP_STARS_SUBF.format(field, chip, subf)
    if os.path.isfile(filename):
        comp_stars = np.loadtxt(filename)
    else:
        comp_stars = generate_comp_stars(field, chip, subf)

    comp_star_obj = []

    star_pos = np.loadtxt(
        constants.DIFFERENCE_IMG_DIR.format(asteroid_id=asteroid_id)
        + "/star_positions{}_{}_{}".format(field, chip, subf))
    for line in comp_stars:
        star = ComparisonStar(line[1], line[2], ufloat(line[3], line[4]))
        star.line = find_closest_star(star.x, star.y, star_pos)
        if star.line is not None:
            comp_star_obj.append(star)
    comp_star_dict["{}-{}-{}".format(field, chip, subf)] = comp_star_obj
    return comp_star_obj[:]


def find_closest_star(starx, stary, star_pos):
    """ Returns the python index of the best matching star in star_pos """
    star_ = np.array([[starx, stary]])
    t = star_pos - star_
    t_ = t[:, 0]**2 + t[:, 1]**2
    index = t_.argmin()
    if t_[index] > MAX_POS_MISMATCH**2:
        return None
    return index


def generate_comp_stars(field, chip, subf, num_stars=MAX_COMP_STARS,
                        write_to_file=True, validate=False):
    """ Helps the user to generate a file for a reference image
    with a list of comparison stars on it """

    print("Generating comp stars")

    comp_file = constants.PHOTCALIB.format(field=field, chip=chip)
    try:
        stars = np.loadtxt(comp_file, usecols=(0, 1, 2, 3, 4))
    except Exception:
        comp_file = comp_file + ".gz"
        stars = np.loadtxt(comp_file, usecols=(0, 1, 2, 3, 4))

    stars = stars[(stars[:, 4] < MAX_MAG_ERROR) & (stars[:, 3] < MAX_MAG)]
    np.random.shuffle(stars)
    print("Possible match stars {}".format(len(stars)))

    comp_stars = []

    imgfile = constants.REFERENCE_SUBF_FORMAT.format(field, chip, subf)

    if validate:
        d = pyds9.DS9()
        d.set("frame delete all")
        d.set("frame new")
        d.set('file ' + imgfile)
        d.set('scale zscale')

    for star in stars:
        id, x, y, mag, magerr = star
        subf_, x, y = get_subf_and_coordinates(x, y)
        if subf_ != subf:
            print("Wrong subf")
            continue
        if validate:
            print("id, x, y, mag, magerr, msky, and so forth")
            print(star)
            print("new x {} new {}".format(x, y))
            spec = 'circle ' + str(x) + ' ' + str(y) + ' 6'
            cmd = 'regions command {' + spec + '}'
            d.set(cmd)
            d.set("pan to %s %s physical" % (x + 1, y + 1))
            d.set("zoom to 3")

            i = raw_input("Good Comparison Star? (RET or y is yes)")
            if len(i) == 0 or i == 'y':
                print("Including star")
                comp_stars.append((id, x, y, mag, magerr))
        else:
            comp_stars.append((id, x, y, mag, magerr))
        if len(comp_stars) > num_stars:
            break

    if len(comp_stars) < MIN_COMP_STARS:
        print("Error: Not enough comparison stars")

    if write_to_file:
        filename = constants.COMP_STARS_SUBF.format(field, chip, subf)
        np.savetxt(filename, comp_stars)

    return comp_stars


def get_transform_coefficents(field, chip):
    """ Get the generated coeffiecents to transform MOA-Red instrumental
    magnitudes to Cousins I magnitudes

    These coeffiecnts were generated by comparing MOA-Red magniutdes with
    the calulcated Cousins I magnitudes from the OGLE dataset. As the
    MOA and OGLE fields are different the mapping doesn't exist for
    all fields.
    """
    # NOTE(amelia): this is an initial and probably incomplete mapping
    # due to such a lack of data, however this will stay as an initial
    # thing to be updated later
    field_mapping = {
        'gb1': ['gb3', 'gb7', 'gb4'],
        'gb2': ['gb1', 'gb3', 'gb7', 'gb4', 'gb5'],
        'gb3': ['gb7', 'gb4', 'gb1', 'gb8'],
        'gb4': ['gb3', 'gb8', 'gb5', 'gb7', 'gb9'],
        'gb5': ['gb4', 'gb9', 'gb6', 'gb8', 'gb10'],
        'gb6': ['gb5', 'gb10', 'gb9', 'gb4', 'gb14', 'gb3'],
        'gb7': ['gb3', 'gb8', 'gb11', 'gb4', 'gb12', 'gb5'],
        'gb8': ['gb5', 'gb10', 'gb9', 'gb4', 'gb14'],
        'gb9': ['gb8', 'gb13', 'gb10', 'gb5', 'gb4', 'gb12'],
        'gb10': ['gb6', 'gb9', 'gb14', 'gb5', 'gb13'],
        'gb11': ['gb7', 'gb12', 'gb8', 'gb5'],
        'gb12': ['gb8', 'gb13', 'gb10', 'gb5', 'gb4', 'gb12'],
        'gb13': ['gb12', 'gb16', 'gb9', 'gb14', 'gb8', 'gb10', 'gb5'],
        'gb14': ['gb13', 'gb17', 'gb10', 'gb15', 'gb9', 'gb18'],
        'gb15': ['gb14', 'gb18', 'gb10', 'gb17', 'gb13', 'gb9', 'gb19', 'gb5'],
        'gb16': ['gb14', 'gb18', 'gb10', 'gb17'],
        'gb17': ['gb16', 'gb14', 'gb18', 'gb14', 'gb15', 'gb13'],
        'gb18': ['gb17', 'gb15', 'gb19', 'gb14', 'gb10', 'gb16', 'gb13', 'gb5'],
        'gb19': ['gb18', 'gb20', 'gb17', 'gb14', 'gb15', 'gb5'],
        'gb20': ['gb19', 'gb18', 'gb17', 'gb14', 'gb15', 'gb5'],
        'gb21': ['gb19', 'gb20', 'gb21', 'gb18', 'gb17', 'gb14', 'gb5'],
        'gb22': ['gb20', 'gb21', 'gb19', 'gb18', 'gb17', 'gb16',
                 'gb14', 'gb5']}
    a, b = None, None

    try:
        file = constants.TRANSFORM_COEF_FILE.format(field=field, chip=chip)
        a, b, c = np.loadtxt(file)
    except IOError:
        for x in field_mapping[field]:
            try:
                file = constants.TRANSFORM_COEF_FILE.format(
                    field=x, chip=chip)
                a, b, c = np.loadtxt(file)
                break
            except IOError:
                continue
    if not a:
        raise Exception("Could not find mapping for {} {}".format(field, chip))
    return a, b


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    fn = constants.DIFF_LIGHTCURVE_FILENAME.format(asteroid_id=asteroid_id)

    lightcurve = []
    with open(fn) as curve:
        for line in curve:
            line = line.split(',')
            lightcurve.append(
                [float(line[0]), line[1], float(line[2]), float(line[3])])

    lightcurve = calibrate_lightcurve(lightcurve, asteroid_id, graph=True)
    lc_filename = constants.DIFF_LIGHTCURVE_FILENAME.format(
        asteroid_id=asteroid_id)
    with open(lc_filename, "wb") as f:
        writer = csv.writer(f)
        writer.writerows(lightcurve)
