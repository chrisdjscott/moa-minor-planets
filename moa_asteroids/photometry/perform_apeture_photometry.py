# Perform apeture photometry on difference images
# makes the assumption that the background of the difference image is nonexistent
# This was Pierre's plan for the difference imaging, I am unsure however
# how to calculate the uncertainties on this
from photutils import CircularAperture, aperture_photometry
from astropy.io import fits
import numpy as np
from matplotlib import pyplot as plt
import sys

from moa_asteroids import constants


def perform_apeture_photometry(asteroid_id, positions_,
                               remove_background=False, radius=6,
                               show_graph=False):
    lightcurve = []
    for line in positions_:
        frame, x, y, fits_image_filename = line.rstrip().split(",")
        x = float(x)
        y = float(y)
        date = None

        source_exp = constants.SOURCE_EXP_FILENAME.format(
            asteroid_id=asteroid_id, frame=frame)

        with fits.open(source_exp) as f:
            date = (float(f[0].header['JDSTART'])
                    + 0.5 * float(f[0].header['EXPTIME'])/86400.0)

        with fits.open(fits_image_filename) as hdul:
            data = hdul[0].data

            # NOTE(amelia): Below is to remove skybackground which doesn't
            # apply in the case of difference images
            if remove_background:
                data -= np.median(data)

            # NOTE(amelia): Circles might not be a great fit as the
            # asteroid moves
            apertures = CircularAperture([x, y], r=radius)
            phot_table = aperture_photometry(data, apertures)

            if show_graph:
                # TODO(amelia): Automatic zoom
                plt.imshow(data, cmap='gray', origin='upper')
                apertures.plot(color="blue")
                plt.show()

            # TODO: Uncertainties
            lightcurve.append((date, phot_table['aperture_sum'][0]))

    return lightcurve


def diff_image_apeture_photometry(astid):
    position_filename = constants.POSITIONS_FILENAME.format(asteroid_id=astid)
    with open(position_filename) as f:
        pos = f.read().splitlines()
    lightcurve = perform_apeture_photometry(astid, pos, show_graph=True)
    return lightcurve


if __name__ == '__main__':
    astid = sys.argv[1]
    lightcurve = diff_image_apeture_photometry(astid)
    fn = constants.APT_LIGHTCURVE_FILENAME.format(asteroid_id=astid)
    np.savetxt(fn.format(astid), lightcurve, fmt='%s', delimiter=',')
