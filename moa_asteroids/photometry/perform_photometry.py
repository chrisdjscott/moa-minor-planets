"""
Nicer access to the perform photmetry command contained inside
make_difference_images.py
"""
import sys

from moa_asteroids.photometry.make_difference_images import \
    perform_photometry_photom_variable_star

if __name__ == '__main__':
    asteroid_id = sys.argv[1]

    perform_photometry_photom_variable_star(asteroid_id)
