"""
Run all photometry steps (from difference imaging through to calibration)
(other files can be run independently if wanted)
"""

import sys

from moa_asteroids.photometry.make_difference_images import (
    make_asteriod_difference_images, perform_photometry_photom_variable_star)
from moa_asteroids.photometry.check_images import (
    automatic_collect_positions, write_to_positions_file)


def do_all_photometry(asteroid_id):
    """
    Run all photometry steps (from difference imaging through to calibration)
    (other files can be run independently if wanted)
    """

    make_asteriod_difference_images(asteroid_id)

    positions = automatic_collect_positions(asteroid_id)
    write_to_positions_file(asteroid_id, positions)

    perform_photometry_photom_variable_star(asteroid_id)


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    do_all_photometry(asteroid_id)
