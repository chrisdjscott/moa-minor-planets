#! /usr/bin/env python3
#

from moa_asteroids import constants
from astroquery.jplhorizons import Horizons

from astropy.time import Time, TimeDelta

import sys
import os
import requests

PASS_GAP_LENGTH = TimeDelta(30, format='jd', scale=None)

# Format for a human readable general statistics frames
ASTEROID_STATS_FMT = """Name: {name}
ID: {asteroid_id}
Orbital Classification: {orbit_classification}
Absolute Magnitude: {mag}
Rotation Period: {rotation_period}

Total Passes: {num_passes}
Total Frames: {total_frames}
Pass Lengths: {pass_lengths}

Passes:
{passes_s}
"""


class Pass(object):

    def __init__(self, asteroid_id):
        self.frames = []
        self.dates = []
        self.asteroid_id = asteroid_id

    def add_frame(self, frame, date):
        self.frames.append(frame)
        self.dates.append(date)

    def date_length(self):
        if not self.dates:
            return 0
        diff = self.dates[-1] - self.dates[0]
        return diff.jd

    def frame_length(self):
        return len(self.frames)

    @property
    def start(self):
        return self.dates[0]

    @property
    def end(self):
        return self.dates[-1]

    def get_apparrent_mag(self):
        """ Get the apparent magnitude from JPL HORIZONS for a given date """
        # TODO: Change to astroquery.jpl
        asteroid = Horizons(id=self.asteroid_id, epochs=self.start.jd)
        eph = asteroid.ephemerides()
        return eph['V'][0]

    def __repr__(self):
        return "<Pass: frames: {} dates: {}>".format(self.frames, self.dates)

    def __str__(self):
        return ("Start Frame: {} End Frame: {}\n"
                "Start Date: {} End Date: {}\n"
                "Apparent V Magnitude: {} \n"
                "Frames: {} \nLength(days): {}").format(
                self.frames[0], self.frames[-1],
                self.dates[0].datetime, self.dates[-1].datetime,
                self.get_apparrent_mag(),
                self.frame_length(), self.date_length())


def base_stats(astid, check_downloaded=False, verbose=True):
    passes = get_passes(astid, check_downloaded)
    num_passes = len(passes)
    length_passes = [p.date_length() for p in passes]
    num_frames = [p.frame_length() for p in passes]
    av_length_passes = average(length_passes)
    av_num_frames = average(num_frames)
    print("Asteroid {}: Passes {} Total Frames: {} "
          "Frame length: {} Time Length: {}".format(
            astid, num_passes, sum(num_frames),
            av_num_frames, av_length_passes))
    return num_passes, length_passes, num_frames


def get_passes(astid, check_downloaded=False, time=True):
    # Time defines whether it is a time based pass or a field + chip based
    # pass

    f = open(constants.TRACK_FILENAME.format(asteroid_id=astid))

    # A random early date to ensure that the first pass gets created
    previous_date = Time('1999-01-01T00:00:00.123456789', format='isot')
    current_pass = Pass(astid)
    previous_field_chip = None
    passes = []

    for line in f:
        frame, date, exposure = line.split()[:3]
        if check_downloaded:
            if not os.path.isfile(
                    constants.SOURCE_EXP_FILENAME.format(asteroid_id=astid)):
                continue

        date = Time(float(date), format='jd')
        if time:

            if date - previous_date >= PASS_GAP_LENGTH:
                current_pass = Pass(astid)
                current_pass.add_frame(frame, date)
                passes.append(current_pass)
            else:
                current_pass.add_frame(frame, date)
            previous_date = date
        else:
            field_chip = frame[-8:]
            if field_chip != previous_field_chip:
                current_pass = Pass(astid)
                current_pass.add_frame(frame, date)
                passes.append(current_pass)
            else:
                current_pass.add_frame(frame, date)
            previous_field_chip = field_chip

    f.close()

    return passes


def average(l):
    if not l:
        return None
    return float(sum(l))/len(l)


def get_jpl_details(asteroid_id):
    # Get details from the JPL small body database browser
    # TODO: Change to astroquery.jpl

    r = requests.post(
        'https://ssd.jpl.nasa.gov/sbdb_query.cgi',
        data={"obj_group": "all", "obj_kind": "ast", "obj_numbered": "num",
              "OBJ_field": 0, "OBJ_op": 0, "OBJ_value": "", "OBB_value": "",
              "ORB_field": 0, "ORB_op": 0, "ORB_value": "",
              "c1_group": "OBJ", "c1_item": "Ad", "c1_op": "=",
              "c1_value": asteroid_id, "c_fields": "AdAeAiApAsCi",
              "table_format": "CSV", "max_rows": 10,
              "format_option": "comp", "query": "Generate Table",
              ".cgifields": ['format_option', 'field_list', 'obj_kind',
                             'obj_group', 'obj_numbered', 'ast_orbit_class',
                             'table_format', 'OBJ_field_set', 'ORB_field_set',
                             'preset_field_set', 'com_orbit_class']})
    values = r.content.split("\n")[1].split(',')
    return {'name': values[1],
            'mag': values[2],
            'orbit_classification': values[5],
            'rotation_period': values[4] or 'unknown'}


def full_asteroid_stats(asteroid_id, write=True, short=False):
    details = get_jpl_details(asteroid_id)
    details['asteroid_id'] = asteroid_id

    passes = get_passes(asteroid_id, time=False)
    details['total_frames'] = sum([x.frame_length() for x in passes])
    details['num_passes'] = len(passes)
    if not short:
        details['passes_s'] = "\n\n".join([str(x) for x in passes])
    else:
        details['passes_s'] = ""
    details['pass_lengths'] = [x.frame_length() for x in passes]
    c = ASTEROID_STATS_FMT.format(**details)
    if write:
        print(c)
        filename = constants.STATS_FILENAME.format(asteroid_id=asteroid_id)
        with open(filename, 'w') as f:
            f.write(c)
    details['passes'] = passes
    return details


if __name__ == '__main__':
    try:
        astid = sys.argv[1]
    except Exception:
        astid = 1
    full_asteroid_stats(astid, short=True)
