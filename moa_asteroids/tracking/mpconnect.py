#! /usr/bin/env python3
#
'''
Automate the connection to the Minor Planet Center Website
'''
import requests

URLBASE = 'https://minorplanetcenter.net/cgi-bin/'


def mpcheck(year, month, day, ra, dec, radius = '5', limit = '20.0',
            verbose=False):
    '''Look-up possible asteroids near the given data and position'''

    # url = URLBASE + 'mpcheck.cgi'
    url = 'https://minorplanetcenter.net/cgi-bin/mpcheck.cgi'

    data = {
        'day':	day,
        'decl':	dec,
        'limit': limit,
        'month': month,
        'mot': 'h',
        'needed': 'f',
        'oc': '500',
        'pdes': 'u',
        'ps': 'n',
        'ra': ra,
        'radius': radius,
        'sort':	'd',
        'TextArea': '',
        'tmot': 's',
        'type':	'p',
        'which': 'pos',
        'year':	year
    }

    resp = requests.post(
        url, data=data,
        headers={"Content-Type": "application/x-www-form-urlencoded"})

    indata = False

    # Parse the response
    results = ''
    lines = resp.text.split("\n")
    for line in lines:
        if verbose:
            print(line)
        line = line.strip('\n').strip()
        if not indata and line.startswith('<pre>'):
            indata = True
            continue
        if indata and line.startswith('</pre>'):
            indata = False
            continue
        if indata: # and line.startswith('('):
            results += line + '\n'

    return results


def mpeph(astid, datestart, utoff, ndates, step, obscode='474'):
    '''Extract asteroid ephemerides.
    ndates = number of ephemeris points
    step = spacing in days between ephemeris points
    the default obscode is for MJUO
    '''
    url = 'https://minorplanetcenter.net/cgi-bin/mpeph2.cgi'

    # POST request
    data = {
            'ty': 'e',
            'TextArea': astid,
            'd': datestart,
            'l': str(ndates),
            'i': str(step),
            'u': 'd',
            'uto': ('%12.6f' % utoff),
            'c': obscode,
            'long': '',
            'lat': '',
            'alt': '',
            'raty': 'a',
            's': 't',
            'm': 'm',
            'adir': 'S',
            'oed': '',
            'e': '-2',
            'resoc': '',
            'tit': '',
            'bu': '',
            'ch': 'c',
            'ce': 'f',
            'js': 'f'
            }

    resp = requests.post(
        url, data=data,
        headers={"Content-Type": "application/x-www-form-urlencoded"})

    result = ''
    lines = resp.text.split("\n")
    for line in lines:
        if line.startswith('2'):
            result += line
            result += line + "\n"

    return result


if __name__ == '__main__':
    # Asteroid found on this exposure380316
    # B37609-gb5-R 2456092.90443 2012 6 14 9 42 22 9.70624800771
    ra = '17 55 34.08'
    dec = '-29 30 23.35'
    year = '2012'
    month = '06'
    day = 14
    utoff = 9.70624800771
    fday = str(day + utoff / 24.0)

    # Check MPC for any asteroids near this date and time
    # Should turn out to be #380316
    res = mpcheck(year, month, fday, ra, dec, radius='5') # radius in arc minutes
    print(res)
