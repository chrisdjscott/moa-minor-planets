#! /usr/bin/env python3

import sys
import math
import time
from astropy.time import Time
from datetime import datetime


def get_date_now():
    # NOTE(amelia): unused

    return time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime())


def cal2jd(year, month, day, hour, min, sec):
    t = Time(datetime(int(year), int(month), int(day), int(hour), int(min), int(sec)))
    return t.jd


def datetime_to_jd(datetime, separator='T'):
    # NOTE(amelia): unused
    datestr, timestr = datetime.split(separator)
    year, month, day = datestr.split('-')
    hour, min, sec = timestr.split(':')

    return cal2jd(year, month, day, hour, min, sec)


def jd2cal(jd, ndp=2):
    '''Converts JD to a human readable calendar date'''
    t = Time(jd, format='jd').datetime

    jdf = jd + 0.5 - math.floor(jd + 0.5)
    return t.year, t.month, t.day + jdf


if __name__ == '__main__':

    jd = float(sys.argv[1])
    cal = jd2cal(jd)
    hr = 24 * (cal[2] - math.floor(cal[2]))
    mn = 60 * (hr - math.floor(hr))
    sec = 60 * (mn - math.floor(mn))

    print(cal)
    print(hr, mn, sec)
