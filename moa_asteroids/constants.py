from moa_asteroids.settings import DATAP, CALIBP, PYDIA_DIR
import os

TEST_DATA_DIR = DATAP + os.path.sep + 'test'

FLAT_INFO_DIRS = [
    DATAP + os.path.sep + 'info/flat_round',
    DATAP + os.path.sep + 'info/flat']

DARK_INFO_DIRS = [DATAP + os.path.sep + 'info/dark', ]

FLAT_FILE_DIR = DATAP + os.path.sep + 'flat'
DARK_FILE_DIR = DATAP + os.path.sep + 'dark'

FLAT_MASTER_DIR = DATAP + os.path.sep + 'flat_master'
DARK_MASTER_DIR = DATAP + os.path.sep + 'dark_master'

DARK_DB_FILENAME = DATAP + os.path.sep + 'darks.dat'
FLAT_DB_FILENAME = DATAP + os.path.sep + 'flats.dat'

DARK_DATABASE_PATHS = ["DARK/fit/dark/{}",
                       "DARK/fit/DARK/dark/{}"]
FLAT_DATABASE_PATHS = ["FLAT/fit/FLAT_ROUND/R/{}",
                       "FLAT/fit/FLAT/R/{}"]

INFODIR = DATAP + os.path.sep + 'info/GB'

TRACKING_DIR = DATAP + "/tracking/"

ASTEROID_DIR = DATAP + os.path.sep + 'asteroids/{asteroid_id}'
SOURCE_EXP_DIR = DATAP + os.path.sep + 'asteroids/{asteroid_id}/source'
PROCESSED_EXP_DIR = DATAP + os.path.sep + 'asteroids/{asteroid_id}/processed'
DIFFERENCE_IMG_DIR = DATAP + os.path.sep + 'asteroids/{asteroid_id}/difference'

SOURCE_EXP_FILENAME = SOURCE_EXP_DIR + os.path.sep + "{frame}.fit"
PROCESSED_EXP_FILENAME = PROCESSED_EXP_DIR + os.path.sep + "{frame}.fit"
PROCESSED_SUBF_FILENAME = PROCESSED_EXP_DIR + os.path.sep + "{frame}-{subf}.fit"
DIFFERENCE_IMG_FILENAME = DIFFERENCE_IMG_DIR + os.path.sep + "d_{frame}.fit"
DIFFERENCE_SUBF_FILENAME = DIFFERENCE_IMG_DIR + os.path.sep + "d_{frame}-{subf}.fit"
MASK_SUBF_FILENAME = DIFFERENCE_IMG_DIR + os.path.sep + "z_{frame}-{subf}.fit"

REG_IMG_FILENAME = DIFFERENCE_IMG_DIR + os.path.sep + "r_{frame}.fit"
REG_SUBF_FILENAME = DIFFERENCE_IMG_DIR + os.path.sep + "r_{frame}-{subf}.fit"
DIFFERENCE_IMG_FLUX = DIFFERENCE_IMG_DIR + os.path.sep + "{frame}.fit.flux"


SEEING_LOG_FMT = "http://www.isee.nagoya-u.ac.jp/~moastaff/Online_Log/SeeingLog/seeinglog-{year}{month:02}{day:02}.txt"

REFERENCE_DIR = DATAP + os.path.sep + "ref"

TRACK_FILENAME = DATAP + os.path.sep + "asteroids/{asteroid_id}/track.dat"
EPHEM_FILENAME = DATAP + os.path.sep + "asteroids/{asteroid_id}/ephem.dat"
STATS_FILENAME = DATAP + os.path.sep + "asteroids/{asteroid_id}/stats.txt"
POSITIONS_FILENAME = DATAP + os.path.sep + "asteroids/{asteroid_id}/positions.dat"

DIFF_LIGHTCURVE_FILENAME = DATAP + os.path.sep + 'asteroids/{asteroid_id}/difflightcurve.csv'
APT_LIGHTCURVE_FILENAME = DATAP + os.path.sep + 'asteroids/{asteroid_id}/aptlightcurve.csv'

DIFF_LC_REDUCED_FILENAME = DATAP + os.path.sep + 'asteroids/{asteroid_id}/difflightcurve_red.csv'

FINAL_OUTPUT_FILE = DATAP + os.path.sep + "export/{asteroid_id}_out.csv"

# Filename for reduced lightcurve with updated uncertainties
DIFF_LC_REDUCED_UN_FILENAME = DATAP + os.path.sep + 'asteroids/{asteroid_id}/difflightcurve_red_un.csv'

REFERENCE_IMAGE_FORMAT = DATAP + "/ref/" + "ref-{}-R-{}.fit"
REFERENCE_SUBF_FORMAT = DATAP + "/ref/" + "ref-{}-R-{}-{}.fit"


PHOTCALIB = CALIBP + '/{field}/ref-{field}-R-{chip}.dat'
TRANSFORM_COEF_FILE = CALIBP + '/{field}/moa-ogle-{field}-R-{chip}.coef'
COMP_STARS = DATAP + "/comp_stars/{}-R-{}.dat"
COMP_STARS_SUBF = DATAP + "/comp_stars/{}-R-{}-{}.dat"


STATUS_FILE = DATAP + os.path.sep + "status.txt"

# Subframe size definition
XMAX = 2048
YMAX = 4096
BORDER = 40
SUBFRAME_SIZE = 512

# Subframes for checking homegenity of dark and flat frames
DARK_FLAT_SUBFRAME_SIZE = 128

PROCESSED_ASTEROIDS = DATAP + os.path.sep + 'already_processed_asteroids.txt'
BAD_ASTEROIDS = DATAP + os.path.sep + 'bad_asteroids.txt'

# Subframes that are known to currrently give breaking errors
# format (field, chip, subf)

# Known issues are:
# PyDIA memory errors (free) -> should be fixed
# IRAF failing at a psf fit
# IRAF just freezing during the psf fitting procedure

BAD_SUBFRAMES = {
    ('gb17', '10', '2_2'): None,  # ast 2010, psf fitting freezes
    ('gb11', '10', '2_0'): None,  # ast 2143 iraf freeze
    ('gb11', '8', '0_4'): None, # ast 2756 psf fitting freeze
    ('gb8', '2', '0_7'): None, # ast ?? iraf freeze
}
