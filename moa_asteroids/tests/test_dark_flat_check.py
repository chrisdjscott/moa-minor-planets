# Very basic automated testing for the flat dark check

# Load a couple of files (either from the database or already saved)
# run them through the code and check pass fail
import pyds9
import unittest
import os
import sys

from moa_asteroids.processing import check_dark_and_flat_frame
from moa_asteroids.setup_bucket import MOA_DB
from moa_asteroids import constants

from astropy.io import fits

MANUAL_TEST = False


def display(files):
    """ Show a list of files in ds9 """
    viewer = pyds9.DS9()

    viewer.set("frame delete all")
    for file in files:
        viewer.set("frame new")
        viewer.set("fits %s" % file)
        viewer.set("zoom to fit")
        viewer.set('zscale')


def download_dark_flat():
    # frame names
    # folder name

    flats = [['F1758-15-R', '1'], ['F2988-121-R', '10']]
    darks = [['D1096-60', '1'], ['D1048-60', '10']]

    for d, chip in darks:
        for fmt in constants.DARK_DATABASE_PATHS:
            for key in MOA_DB.list(fmt.format(d) + "-", ""):
                filename = (constants.TEST_DATA_DIR + os.path.sep +
                            key.name.split("/")[-1])
                bad_filename = (constants.TEST_DATA_DIR + os.path.sep +
                                "bad" + key.name.split("/")[-1])

                if filename.endswith("-{}.fit".format(chip)):
                    key.get_contents_to_filename(filename)

                    # Creade bad versions
                    hdul = fits.open(filename)

                    # modify to give a bad result
                    hdul[0].data[200:300, 450:600] = 200

                    hdul.writeto(bad_filename, overwrite=True)

    for d, chip in flats:
        for fmt in constants.FLAT_DATABASE_PATHS:
            for key in MOA_DB.list(fmt.format(d) + "-", ""):
                filename = (constants.TEST_DATA_DIR + os.path.sep +
                            key.name.split("/")[-1])

                if filename.endswith("-{}.fit".format(chip)):
                    key.get_contents_to_filename(filename)


class TestDarkFlatCheck(unittest.TestCase):

    def test_good_dark(self):
        files = ['D1048-60-a-10.fit', 'D1048-60-b-10.fit', 'D1048-60-c-10.fit']
        files = [constants.TEST_DATA_DIR + os.path.sep + x for x in files]
        self.assertTrue(check_dark_and_flat_frame.check_dark_files(files))

        files = ['D1096-60-a-1.fit', 'D1096-60-b-1.fit', 'D1096-60-c-1.fit']
        files = [constants.TEST_DATA_DIR + os.path.sep + x for x in files]
        self.assertTrue(check_dark_and_flat_frame.check_dark_files(files))

    def test_good_flat(self):
        files = ['F1758-15-R-a-1.fit', 'F1758-15-R-b-1.fit',
                 'F1758-15-R-c-1.fit']
        files = [constants.TEST_DATA_DIR + os.path.sep + x for x in files]
        self.assertTrue(check_dark_and_flat_frame.check_flat_files(files))

        files = ['F2988-121-R-a-10.fit', 'F2988-121-R-b-10.fit',
                 'F2988-121-R-c-10.fit']
        files = [constants.TEST_DATA_DIR + os.path.sep + x for x in files]
        self.assertTrue(check_dark_and_flat_frame.check_flat_files(files))

    def test_bad_dark(self):
        files = ['D1048-60-a-10.fit', 'D1048-60-b-10.fit', 'D1048-60-c-10.fit']
        files = [constants.TEST_DATA_DIR + os.path.sep + "bad" + x
                 for x in files]
        self.assertFalse(check_dark_and_flat_frame.check_dark_files(files))

        files = ['D1096-60-a-1.fit', 'D1096-60-b-1.fit', 'D1096-60-c-1.fit']
        files = [constants.TEST_DATA_DIR + os.path.sep + "bad" + x
                 for x in files]
        self.assertFalse(check_dark_and_flat_frame.check_dark_files(files))


if __name__ == '__main__':
    if sys.argv[-1] == 'download':
        download_dark_flat()
    unittest.main()
