from astropy.io import fits
from astropy.stats import sigma_clipped_stats

from moa_asteroids import constants


def check_dark_subframe(image, reference_mean, row, column, multiplier=0.05):
    """ Check that the subframe mean is within 3% of the whole image mean"""
    mean, median, std = sigma_clipped_stats(
        crop(image, row, column), sigma=3.0)
    return bool(abs(mean - reference_mean) < mean*multiplier)


def check_flat_subframe(image, reference_mean, row, column):
    """ Check that the subframe mean is within 20% of the whole image mean"""
    mean, median, std = sigma_clipped_stats(
        crop(image, row, column), sigma=3.0)
    return abs(mean - reference_mean) < reference_mean*0.2


def crop(image, row, column):
    """ It takes a dark frame and crops it in 16*16 subframes
        and return data from the square in the position (row, column) """

    xstart = column * constants.DARK_FLAT_SUBFRAME_SIZE
    xfinish = (column + 1) * constants.DARK_FLAT_SUBFRAME_SIZE
    ystart = row * constants.DARK_FLAT_SUBFRAME_SIZE
    yfinish = (row + 1) * constants.DARK_FLAT_SUBFRAME_SIZE

    # xstart, ystart, xend, yend = get_subf_edges(row, column)
    # open fits file
    return image[ystart:yfinish, xstart:xfinish]


def check_flat_files(files):
    ncol = constants.XMAX/constants.DARK_FLAT_SUBFRAME_SIZE
    nrow = constants.YMAX/constants.DARK_FLAT_SUBFRAME_SIZE

    return True

    for file in files:
        with fits.open(file) as hdul:
            data = hdul[0].data

            reference_mean, _, _ = sigma_clipped_stats(data, sigma=3.0)
            for row in range(1, nrow):
                # Bottom row sometimes has issues when at the edge just
                # due to the optical propetries of the system, there is
                # less light at the edges of the ccd, the bottom corner
                # is one well known location of this
                for column in range(ncol):
                    if not check_flat_subframe(data, reference_mean,
                                               row, column):
                        print("Rejecting flat file {}".format(file))
                        return False
    return True


def check_dark_files(files):
    ncol = constants.XMAX/constants.DARK_FLAT_SUBFRAME_SIZE
    nrow = constants.YMAX/constants.DARK_FLAT_SUBFRAME_SIZE

    for file in files:
        with fits.open(file) as hdul:
            data = hdul[0].data

            reference_mean, _, _ = sigma_clipped_stats(data, sigma=3.0)

            for row in range(1, nrow):
                # Bottom row may have additional bleed, this is normal
                # and should not be checked for
                for column in range(ncol):
                    if not check_dark_subframe(data, reference_mean,
                                               row, column):
                        print("Rejecting dark file {}".format(file))
                        return False
    return True
