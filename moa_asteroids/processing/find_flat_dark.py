import os
from astropy.time import Time
from astropy.nddata import CCDData
from astropy import units as u

from moa_asteroids import constants
from moa_asteroids.tracking.lookup import getnum
from moa_asteroids.setup_bucket import MOA_DB
from moa_asteroids.processing import check_dark_and_flat_frame


from datetime import timedelta

import ccdproc
import requests

import pyds9
TEMPERATURE_MATCH = 2


class MissingFlatDarkException(Exception):
    pass


def get_frame_info(frame, key=None, filename=False):
    if not filename:
        exposure = frame[:-2]
        if frame[-2] == '1':
            exposure = frame[:-3]
        if frame[0] == 'B':
            frame = constants.INFODIR + os.path.sep + exposure + ".info"
    with open(frame) as f:
        if key is None:
            return f.readlines()
        else:
            for line in f:
                if line.startswith(key):
                    return line[10:30].strip("'").strip()


def get_frame_date(frame, filename=False):
    return Time("20" + get_frame_info(frame, "DATE"), format='fits').datetime


def find_flat_master(frame):
    """ For a given GB frame attempt to find a matching flat frame

    Will check the written FLAT_DB_FILE for the date, and then go through the
    info files for a date match, starting with a 90 day match period, and
    slowly increasing the possible distance between dates for the date match.
    """

    # frame = 'B23614-gb21-R-3'
    # get exposure date
    chip = frame[-1]
    if chip == '0':
        chip = '10'
    date = get_frame_date(frame)
    date_m_str = "{:04}-{:02}".format(date.year, date.month)

    # check FLAT_DB_FILENAME for the month
    with open(constants.FLAT_DB_FILENAME) as f:
        for line in f:
            if line.startswith(date_m_str):
                # lineformat: yyyy-mm-dd, run_id, filename, flat_round, or flat
                print("Flat Source: DB File")
                _, run_id, run_exp, _ = line.split(',')
                return check_flat_and_master(run_exp, chip, date,
                                             write_existing=False)
    # NOTE(amelia): Flats are supposed to be taken every 3 months
    # however some are missing for the moa database, the files below
    # will attempt to find a matching flat with
    master = check_files_for_flat(chip, date, 90)
    if master:
        return master
    master = check_files_for_flat(chip, date, 180)
    if master:
        return master
    master = check_files_for_flat(chip, date, 250)
    if master:
        return master

    raise MissingFlatDarkException("Flat image not found for {}".format(frame))


def check_files_for_flat(chip, date, days):
    """ Attempt to find a flat master for the chip where
    abs(framedate - date) < days) """
    for dir in constants.FLAT_INFO_DIRS:
        flatfiles = os.listdir(dir)
        flatfiles.sort(key=lambda x: getnum(x))
        for file in flatfiles:
            filename = os.path.join(dir, file)
            if abs(date - get_frame_date(filename, filename=True)) < timedelta(
                    days=days):
                print("Flat Source: Date match on info files")
                # TODO(amelia): Start with a smaller time period and then
                # increase it
                dframe = "-".join(file.split("-")[:2])
                master = check_flat_and_master(dframe, chip, date)
                if master:
                    return master
            if (get_frame_date(filename, filename=True) - date) > timedelta(
                    days=days):
                # We aren't going to get any futher flats as they will just get
                # later on
                break

        for file in flatfiles:
            filename = os.path.join(dir, file)
            if abs(date - get_frame_date(filename, filename=True)) < timedelta(
                    days=250):
                print("Flat Source: Date match on info files")
                # NOTE(amelia): Flats are supposed to be taken every 3 months
                # TODO(amelia): Start with a smaller time period and then
                # increase it
                dframe = "-".join(file.split("-")[:2])
                master = check_flat_and_master(dframe, chip, date)
                if master:
                    return master
            if (get_frame_date(filename, filename=True) - date) > timedelta(
                    days=250):
                # We aren't going to get any futher flats as they will just get
                # later on
                break


def find_dark_master(frame):
    """ For a given GB frame attempt to find a matching dark frame

    Will check the written DARK_DB_FILE for the date, then the online
    seeing log, then go through the info files for a date match, and
    finally if there are issues with that (missing files etc) it will
    do a temperature + year match.
    """
    chip = frame[-1]
    if chip == '0':
        chip = '10'
    date = get_frame_date(frame)
    date_str = "{:04}-{:02}-{:02}".format(date.year, date.month, date.day)
    temperature = float(get_frame_info(frame, "CCDTEMP"))

    seeing_log_file = constants.SEEING_LOG_FMT.format(
        year=date.year, month=date.month, day=date.day)

    # check DARK_DB_FILENAME for the date
    with open(constants.DARK_DB_FILENAME) as f:
        for line in f:
            if line.startswith(date_str):
                # lineformat: yyyy-mm-dd, run_id, filename, temperature
                _, run_id, dframe, _ = line.split(',')
                print("Dark Source: DB File")
                master = check_dark_and_master(dframe, chip, date,
                                               write_existing=False)
                print(master)
                if master:
                    return master

    r = requests.get(seeing_log_file)
    if r.status_code == 200:
        # Attempt to get that dark
        #
        for line in r.content.split("\n"):
            if line.startswith('#   D'):
                line = line.strip("#").strip()
                run, exposure = line.split()
                if exposure != '60':
                    # NOTE(amelia): running only 60 sec GB fields
                    continue

                # TODO(amelia): Find info file,]
                dframe = "{}-60".format(run)

                print("Dark Source: Seeing log")
                # infofile, chip, date
                master = check_dark_and_master(dframe, chip, date)
                if master:
                    return master
                print("Not from website")
    else:
        for dir in constants.DARK_INFO_DIRS:
            darkfiles = os.listdir(dir)
            darkfiles.sort(key=lambda x: getnum(x))

            for file in darkfiles:
                if '-60-' not in file:
                    # Only wanting 60 second GB exposures
                    continue
                filename = os.path.join(dir, file)
                if date.date == get_frame_date(filename, filename=True).date:
                    print("Dark Source: Date match on info files")
                    master = check_dark_and_master(filename, chip)
                    if master:
                        return master
                if ((date - get_frame_date(filename, filename=True)) >
                        timedelta(days=1)):
                    # NOTE(amelia): Any darks after this date will not meet
                    # the requirement
                    break

    # We couldn't find a dark with a matching date, so will now attempt to
    # find a tempeature match +/1 degrees, during the same year
    # This will also be recoreded in the file database for this date
    # but with a note that it is not the actual file but a fake one

    frame_temp = float(get_frame_info(frame, key="CCDTEMP"))
    for dir in constants.DARK_INFO_DIRS:
        darkfiles = os.listdir(dir)
        darkfiles.sort(key=lambda x: getnum(x))

        for file in darkfiles:
            filename = os.path.join(dir, file)
            if '-60-' not in file:
                # Only wanting 60 second GB exposures
                continue
            frame_temp = float(get_frame_info(filename, key="CCDTEMP",
                                              filename=True))
            dframe = "-".join(file.split("-")[:2])
            if (date.year == get_frame_date(filename, filename=True).year and
                    abs(frame_temp - temperature) < TEMPERATURE_MATCH):
                print("Dark Source: Year + Temperature match on info files")
                master = check_dark_and_master(dframe, chip, date)

                if master:
                    return master
            if get_frame_date(filename, filename=True).year > date.year:
                # We are looking for a year match between the images to
                # account for ccd changes
                break
    raise MissingFlatDarkException(
        "Dark image not found for {}".format(frame))


def check_flat_and_master(frame, chip, date, write_existing=True):
    return check_and_master(frame, chip, date, 'flat', write_existing)


def check_dark_and_master(frame, chip, date, write_existing=True):
    return check_and_master(frame, chip, date, 'dark', write_existing)


def check_and_master(frame, chip, date, type_, write_existing, automated=True):
    """ Download check and create master for flat/dark

    For a given frame (the frame being the run id + exposure and posssibly
    the filter), and chip download the files and attempt to create a master
    checking the files with the user first.
    """

    db_file = None
    file_dir = None
    master_dir = None
    database_paths = None
    check_function = None
    if type_ == 'flat':
        db_file = constants.FLAT_DB_FILENAME
        file_dir = constants.FLAT_FILE_DIR
        master_dir = constants.FLAT_MASTER_DIR
        database_paths = constants.FLAT_DATABASE_PATHS
        check_function = check_dark_and_flat_frame.check_flat_files
    elif type_ == 'dark':
        db_file = constants.DARK_DB_FILENAME
        file_dir = constants.DARK_FILE_DIR
        master_dir = constants.DARK_MASTER_DIR
        database_paths = constants.DARK_DATABASE_PATHS
        check_function = check_dark_and_flat_frame.check_dark_files
    else:
        return  # raise execption()

    if not automated:
        check_function = check_files_for_master

    master_filename = frame + "-{}-master.fit".format(chip)
    if os.path.isfile(master_dir + os.path.sep + master_filename):
        if write_existing:
            # write to file
            # lineformat: yyyy-mm-dd, run_id, infofile, flat_round, or flat
            run_id = None
            with open(db_file, 'a') as f:
                f.write("{},{},{},{}\n".format(date, run_id, frame, None))
        return master_dir + os.path.sep + master_filename

    # if that does exist, return it's path
    files = []

    a_filename = frame + "-a-{}.fit".format(chip)
    if os.path.isfile(file_dir + os.path.sep + a_filename):
        files = [file_dir + os.path.sep + frame +
                 "-{}-{}.fit".format(letter, chip)
                 for letter in ['a', 'b', 'c']]
        if check_function(files):
            if write_existing:
                # write to file
                # lineformat: yyyy-mm-dd, run_id, infofile, flat_round, or flat
                run_id = None
                with open(db_file, 'a') as f:
                    f.write("{},{},{},{}\n".format(date, run_id, frame, None))
            return create_master(files, frame, master_dir, chip)
        else:
            print("File rejected")

    if not files:
        for fmt in database_paths:
            for key in MOA_DB.list(fmt.format(frame) + "-", ""):
                filename = file_dir + os.path.sep + key.name.split("/")[-1]
                if filename.endswith("-{}.fit".format(chip)):
                    key.get_contents_to_filename(filename)
                    files.append(filename)
    if files:
        print("Files downloaded from database {}".format(files))
        if check_function(files):
            run_id = None
            with open(db_file, 'a') as f:
                f.write("{},{},{},{}\n".format(date, run_id, frame, None))
            return create_master(files, frame, master_dir, chip)
            # if that doesn't work download it from the database and
            # then create the flat master from it
        print("File rejected")

    raise MissingFlatDarkException


def display(files):
    """ Show a list of files in ds9 """
    viewer = pyds9.DS9()

    viewer.set("frame delete all")
    for file in files:
            viewer.set("frame new")
            viewer.set("fits %s" % file)
            viewer.set("zoom to fit")


def check_files_for_master(files):
    try:
        display(files)
    except ValueError as e:
        print("Error displaying file {}".format(e))
        return False

    check = raw_input("Is this a good set of images? (RET or y is yes)")
    return True if not check or check[0].lower() == 'y' else False


def create_master(files, frame, master_dir, chip):
    try:
        """ Combine a list of files into a master and save it """
        log = """ HISTORY RAW1:  {}
    HISTORY RAW2: {}
    HISTORY RAW3: {}""".format(*files)

        ccd_list = []
        for filename in files:
            ccd = CCDData.read(filename, unit=u.adu)
            # ccd = ccdproc.gain_correct(ccd, GAIN)
            ccd_list.append(ccd)

        master = ccdproc.combine(ccd_list, method='median', add_keyword=log)
        master_name = master_dir + os.path.sep + frame + "-{}-master.fit".format(
            chip)
        master.write(master_name, overwrite=True)
        print("Master created {} from {}".format(master_name, files))
        return master_name
    except Exception as e:
        raise MissingFlatDarkException(e)
