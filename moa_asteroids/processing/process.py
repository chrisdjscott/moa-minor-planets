from astropy import units as u
from astropy.io import fits
from astropy.nddata import CCDData
import ccdproc
import sys
import os

from moa_asteroids import constants
from moa_asteroids.util import get_subf_edges
from moa_asteroids.processing.find_flat_dark import (
    find_flat_master, find_dark_master, MissingFlatDarkException)


def apply_flat_and_dark(frame, asteroid_id):
    print("Raw frame: {}".format(frame))
    flat_master_name = find_flat_master(frame)
    dark_master_name = find_dark_master(frame)
    print("Dark frame: {}".format(dark_master_name))
    print("Flat frame: {}".format(flat_master_name))
    dark_master = CCDData.read(dark_master_name, unit=u.adu)
    flat_master = CCDData.read(flat_master_name, unit=u.adu)

    source_filename = constants.SOURCE_EXP_FILENAME.format(
        asteroid_id=asteroid_id, frame=frame)
    ccd = CCDData.read(source_filename, unit=u.adu)
    my_log = {"history": "RAW: {raw} DARK: {dark} FLAT: {flat}".format(
        raw=source_filename, dark=flat_master_name, flat=flat_master_name)}

    ccd = ccdproc.ccd_process(ccd,
                              dark_frame=dark_master,
                              exposure_key='EXPTIME',
                              exposure_unit=u.second,
                              master_flat=flat_master,
                              add_keyword=my_log)
    return ccd
    # Save new image


def process_asteroid_images(asteroid_id, check_exisiting=False,
                            do_subf=True):
    """ Perform ccd processing (flats + darks) on asteroid images

    Given an asteroid id it will pull out the images from
    SOURCE_EXP_DIR find matching darks and flats, download these from
    the database if nessicary and then perform processing on them.

    If check existing is True, it will not process files that have
    already been processed.

    If do_subf is true it will crop the images into subfrants, and generate
    a cropped reference image to that subfrant if one doesn't already exist
    """
    with open(constants.TRACK_FILENAME.format(asteroid_id=asteroid_id)) as f:
        folder = constants.PROCESSED_EXP_DIR.format(asteroid_id=asteroid_id)
        src_folder = constants.SOURCE_EXP_DIR.format(asteroid_id=asteroid_id)
        if not os.path.exists(folder):
            os.makedirs(folder)
        exclude_files = []
        if check_exisiting:
            exclude_files = [folder + "/" + x for x in os.listdir(folder)]
        include_files = [src_folder + "/" + x for x in os.listdir(src_folder)]
        print(include_files)
        for line in f:
            line = line.split()
            frame = line[0]
            source_file = constants.SOURCE_EXP_FILENAME.format(
                asteroid_id=asteroid_id, frame=frame)
            if (source_file not in include_files or
                    source_file in exclude_files):
                continue

            _, field, _, chip = frame.split('-')

            if (field, chip, line[7]) in constants.BAD_SUBFRAMES:
                print("Known bad subframe {} skipping".format(
                    (field, chip, line[7])))
                continue

            try:
                ccd = apply_flat_and_dark(frame, asteroid_id)
            except MissingFlatDarkException:
                # TODO(amelia): Maybe log this somewhere?
                # later though, and export scripts will pick this up
                # as an 'UNKNOWN' cujrrently
                print("Error finding valid dark and flat for frame {}")
                continue

            filename = constants.PROCESSED_EXP_FILENAME.format(
                asteroid_id=asteroid_id, frame=frame)

            with fits.open(source_file) as f:
                # applying ccd proccessing can mean that some saturated
                # pixels no longer show up as saturated, this is a fix
                # for that
                source_data = f[0].data
                ccd.data[source_data > 40000] = 65535

            if do_subf:
                subf = line[7]
                xstart, ystart, xend, yend = get_subf_edges(subf)

                ccd.data = ccd.data[ystart:yend, xstart:xend]
                filename = constants.PROCESSED_SUBF_FILENAME.format(
                    asteroid_id=asteroid_id, frame=frame,
                    subf=subf)

                _, field, _, chip = frame.split("-")
                crop_ref(field, chip, subf)

            ccd.write(filename, overwrite=True)


def crop(in_file, out_file, subf):
    xstart, ystart, xend, yend = get_subf_edges(subf)
    # open fits file
    with fits.open(in_file) as hdul:
        data = hdul[0].data
        data = data[ystart:yend, xstart:xend]
        header = hdul[0].header
        header['history'] = "Cropped image, cropped to subf {}".format(subf)
        header['NAXIS1'] = xend - xstart
        header['NAXIS2'] = yend - ystart
        new_hdu = fits.PrimaryHDU(data, header)
        new_hdu.writeto(out_file)


def crop_ref(field, chip, subf):
    xstart, ystart, xend, yend = get_subf_edges(subf)

    filename = constants.REFERENCE_IMAGE_FORMAT.format(field, chip)
    new_filename = constants.REFERENCE_SUBF_FORMAT.format(field, chip, subf)

    if not os.path.exists(new_filename):
        crop(filename, new_filename, subf)


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    process_asteroid_images(asteroid_id)
