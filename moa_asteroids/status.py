from moa_asteroids import constants
import os
import sys

"""
For all asteroids compute their status and write it to a file

State options
No frames (in tracking) NO_MATCH
Tracking complete but no other actions have been taken TRACK_COMPLETE
Tracked frames no images MATCH_NO_IMAGES
Images downloaded SOURCE_IMAGES
Images have preprocessing PROCESSED_IMAGES
Difference imaging carried out  HAS_DIFFERENCE_IMAGES
Photometry Carried out LIGHTCURVE_CREATED
Export file created EXPORT_COMPLETE
"""


def find_status(asteroid_id):
    """
    Check what parts of processing images and tracking has happened for
    this asteroid
    """

    e_fn = constants.FINAL_OUTPUT_FILE.format(asteroid_id=asteroid_id)

    if os.path.isfile(e_fn):
        return "EXPORT_COMPLETE"

    # open tracking file and ephem file name
    t_fn = constants.TRACK_FILENAME.format(asteroid_id=asteroid_id)

    if not os.path.isfile(t_fn):
        return "NOT_STARTED"

    if os.stat(t_fn).st_size == 0:
        return "NO_MATCH"

    lc_fn = constants.DIFF_LIGHTCURVE_FILENAME.format(asteroid_id=asteroid_id)
    if os.path.isfile(lc_fn):
        return "LIGHTCURVE_CREATED"

    diff_dir = constants.DIFFERENCE_IMG_DIR.format(asteroid_id=asteroid_id)
    if (os.path.isdir(diff_dir) and
            [f for f in os.listdir(diff_dir) if not f[0] == '.'] != []):
        return "HAS_DIFFERENCE_IMAGES"

    process_dir = constants.PROCESSED_EXP_DIR.format(asteroid_id=asteroid_id)
    if (os.path.isdir(process_dir) and
            [f for f in os.listdir(process_dir) if not f[0] == '.'] != []):
        return "PROCESSED_IMAGES"

    source_dir = constants.SOURCE_EXP_DIR.format(asteroid_id=asteroid_id)
    if (os.path.isdir(source_dir) and
            [f for f in os.listdir(source_dir) if not f[0] == '.'] != []):
        return "SOURCE_IMAGES"
    elif os.path.isdir(source_dir):
        return "MATCH_NO_IMAGES"
    else:
        return "TRACK_COMPLETE"


def get_num_frames(asteroid_id):
    try:
        t_fn = constants.TRACK_FILENAME.format(asteroid_id=asteroid_id)
        return len(open(t_fn).readlines())
    except IOError:
        # File does not exist
        return None


def get_num_images(asteroid_id):
    if not os.path.isdir(
            constants.SOURCE_EXP_DIR.format(asteroid_id=asteroid_id)):
        return None
    i = 0
    for f in os.listdir(
            constants.SOURCE_EXP_DIR.format(asteroid_id=asteroid_id)):
        if f.endswith(".fit"):
            i += 1
    return i


def all_asteroid_status(ignore_no_match=True):
    """
    For all asteroids that any processing has been completed for find their
    current status and number of possible frames
    """

    ast_list = []
    for f in os.listdir(constants.ASTEROID_DIR.format(asteroid_id='')):
        try:
            ast_list.append(int(f))
        except ValueError:
            pass

    ast_list.sort()

    fmt = "{:6} {:22} {:4} {:4}"
    with open(constants.STATUS_FILE, "w") as f:
        for asteroid in ast_list:
            status = find_status(asteroid)
            if ignore_no_match and status == 'NO_MATCH':
                continue
            frames = get_num_frames(asteroid)
            num_images = get_num_images(asteroid)
            out = fmt.format(asteroid, status, frames, num_images)
            print(out)
            f.write(out + '\n')
    f.close()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        status = find_status(sys.argv[1])
        print("Asteroid {} Status: {}".format(sys.argv[1], status))
    else:
        all_asteroid_status(False)
