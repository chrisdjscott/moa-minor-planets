"""
A more specific batch script that takes into account context:

For a given list of asteroids (for which tracking has been completed as per
moa-util-scripts) it will:
1. Check if an export file exists
2. Check if difference images exist. If so run batch from there
3. Check if processed images exist, if so run batch from there
4. If not it will start from downloading images

5. Once export has completed it will delete all image files and move the
tracking files/rest of the folder to DATAP/asteroids/complete/{asteroid_id}

6. start on the next asteroid

By default takes list of asteroids from standard in

reccomended use:
`cat asteroids_list.txt | python better_batch.py`
"""

from moa_asteroids.batch import batch
from moa_asteroids import constants

import os
import shutil
import sys


def clean_asteroid_files(asteroid_id):
    # this is a function as it could be used to do more detailed things
    # ie keep some files later(

    print("DELETE ", constants.ASTEROID_DIR.format(asteroid_id=asteroid_id))
    shutil.rmtree(constants.ASTEROID_DIR.format(asteroid_id=asteroid_id))


def run_asteroid(asteroid_id):
    """ Context aware batch process a single asteroid """

    if os.path.isfile(
            constants.FINAL_OUTPUT_FILE.format(asteroid_id=asteroid_id)):
        # asteroid has already been processed
        return False

    diff_dir = constants.DIFFERENCE_IMG_DIR.format(asteroid_id=asteroid_id)
    processed_dir = constants.PROCESSED_EXP_DIR.format(asteroid_id=asteroid_id)

    if os.path.exists(diff_dir) and os.path.isdir(diff_dir):
        # some difference imaging has been completed
        batch([asteroid_id, ], track=False,  download=False, process_=False,
              difference_photometry=True, export_=True)
    elif os.path.exists(processed_dir) and os.path.isdir(processed_dir):
        # some processing has been completed
        batch([asteroid_id, ], track=False,  download=False, process_=True,
              difference_photometry=True, export_=True)
    elif os.path.exists(
            constants.TRACK_FILENAME.format(asteroid_id=asteroid_id)):
        # tracking has been completed
        batch([asteroid_id, ], track=False,  download=True, process_=True,
              difference_photometry=True, export_=True)
    else:
        # no processing on this asteroid has been completed
        batch([asteroid_id, ], track=True,  download=True, process_=True,
              difference_photometry=True, export_=True)

    # Remove existing asteroid files
    clean_asteroid_files(asteroid_id)
    return True


if __name__ == '__main__':

    asteroid_ids = [x.rstrip() for x in sys.stdin.readlines()]

    for i in asteroid_ids:
        run_asteroid(i)
